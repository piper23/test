import Header from "Components/Header";
import React from "react";
import Tasks from "./Modules/Container";

function App() {
  return (
    <div className="App">
      <Header />

      <div className="container-fluid py-5 bg-dark full-body">
        <Tasks />
      </div>
    </div>
  );
}

export default App;
