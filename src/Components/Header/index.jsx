import React, { useEffect, useState } from "react";
import { useContext } from "react";
import { Task } from "Store/TaskStore";

const Header = () => {
  const { state } = useContext(Task);

  const [numberChanged, setNumberChanged] = useState(true);

  useEffect(() => {
    setNumberChanged(true);
    const timer = setTimeout(() => {
      setNumberChanged(false);
    }, 1500);
    return () => clearTimeout(timer);
  }, [state.tasks.count]);

  return (
    <header className="container-fluid d-flex justify-content-between  align-items-center">
      <h1>Tasker</h1>
      <p
        className={`font-weight-bold  mb-0 bg-dark text-white py-1 px-3 rounded-xl mr-3 ${
          numberChanged ? "hgrow" : ""
        }`}
      >
        {state.tasks.count}
      </p>
    </header>
  );
};
export default Header;
