import React, { useState } from "react";
import { Task } from "../Store/TaskStore";

import Rows from "../Views/Rows";
import SearchForm from "../Views/SearchForm";
import AddForm from "../Views/AddForm";
import rowTitles from "./rowTitles";
import {
  UPDATE_TASKS,
  REMOVE_TASK,
  NEW_TASK_ITEM,
  UPDATE_TASK_ITEM,
} from "../Store/actions";
import { DragDropContext, Droppable } from "react-beautiful-dnd";
import { getListStyle, handleDragEnd } from "./utils/drag";

function Tasks() {
  const { state, dispatch } = React.useContext(Task);
  const [searchTerm, setSearchTerm] = useState("");

  const updateTasks = (payload) => {
    return dispatch({
      type: UPDATE_TASKS,
      payload,
    });
  };
  const addTask = (payload, text) => {
    return dispatch({
      type: NEW_TASK_ITEM,
      payload,
      text,
    });
  };
  const updateTask = (payload) => {
    return dispatch({
      type: UPDATE_TASK_ITEM,
      payload,
    });
  };
  const removeTask = (payload) => {
    return dispatch({
      type: REMOVE_TASK,
      payload,
    });
  };

  const getList = (key) => state.tasks[key];
  const onDragEnd = (result) => handleDragEnd({ result, updateTasks, getList });

  const getStageData = (key) => {
    if (searchTerm === "") {
      return state.tasks[key];
    }
    return state.tasks[key].filter((t) => {
      const filter = searchTerm.toUpperCase();
      if (t.text && t.text.toUpperCase().indexOf(filter) > -1) {
        return true;
      }
      return false;
    });
  };

  return (
    <div className="Tasks">
      <div className="row">
        <div className="col-md-4">
          <AddForm addTask={addTask} />
        </div>
        <div className="col-md-4 offset-md-4">
          <SearchForm
            updateSearchTerm={setSearchTerm}
            searchTerm={searchTerm}
          />
        </div>
      </div>

      <div className="py-4 row">
        <DragDropContext onDragEnd={onDragEnd}>
          {rowTitles.map(({ key, title }) => (
            <div className={`col-md-${12 / rowTitles.length}`} key={key}>
              <div className="px-2 py-1 bg-white rounded ">
                <div className="row px-2">
                  <div className="col-10">
                    <h3>
                      {title}{" "}
                      <small className="text-muted h5 ">
                        ({getStageData(key).length})
                      </small>{" "}
                    </h3>
                  </div>
                  {/* <div className="col-2">
                    <div
                      className="add-btn btn"
                      onClick={() => addTask(key)}
                    >
                      <Icon type="add" />
                    </div>
                  </div> */}
                </div>
                <Droppable droppableId={key}>
                  {(provided, snapshot) => (
                    <div
                      ref={provided.innerRef}
                      style={getListStyle(snapshot.isDraggingOver)}
                      className={`bg-dark`}
                    >
                      <Rows
                        updateTask={updateTask}
                        removeTask={removeTask}
                        stage={key}
                        title={title}
                        data={getStageData(key)}
                      />
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              </div>
            </div>
          ))}
        </DragDropContext>
      </div>
    </div>
  );
}

export default Tasks;
