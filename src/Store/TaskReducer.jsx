import { v4 as uuidv4 } from "uuid";
import {
  NEW_TASK_ITEM,
  UPDATE_TASK_ITEM,
  UPDATE_TASKS,
  REMOVE_TASK,
} from "./actions";
import rowTitles from "../Modules/rowTitles";

export const getInitialState = (rowTitles) => ({
  ...rowTitles.reduce((mem, { key }) => {
    mem[key] = [];
    return mem;
  }, {}),
  searchTerm: "",
  count: 0,
});

const initialState = getInitialState(rowTitles);

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case NEW_TASK_ITEM: {
      const now = new Date().getTime();
      const stage = action.payload;
      return {
        ...state,
        [stage]: [
          {
            text: action.text,
            id: uuidv4(),
            editMode: false,
            created: now,
            updated: now,
          },
          ...state[stage],
        ],
        count: state.count + 1,
      };
    }

    case UPDATE_TASK_ITEM: {
      var { taskID, text, stage } = action.payload;
      stage = "progress";
      const now = new Date().getTime();
      return {
        ...state,
        [stage]: state[stage].map((item) => {
          if (item.id !== taskID) {
            return item;
          }
          return {
            ...item,
            text,
            editMode: false,
            updated: now,
          };
        }),
      };
    }

    case UPDATE_TASKS: {
      return {
        ...state,
        ...action.payload,
      };
    }

    case REMOVE_TASK: {
      const { stage, taskID } = action.payload;
      return {
        ...state,
        [stage]: state[stage].filter((t) => t.id !== taskID),
        count: state.count - 1,
      };
    }

    default:
      return state;
  }
}
