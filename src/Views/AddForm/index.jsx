import React, { useState } from "react";
import rowTitles from "Modules/rowTitles";

const AddForm = ({ addTask }) => {
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState(true);
  const [dropDown, setDropDown] = useState(rowTitles[0].key);
  const addCard = (e) => {
    e.preventDefault();
    setNameError(false);
    //validation
    if (name) {
      addTask(dropDown, name);
      setName("");
    } else {
      setNameError(true);
    }
  };

  return (
    <form onSubmit={(e) => addCard(e)}>
      <div className="d-flex justify-content-between flex-wrap">
        <div className="pr-3 mb-3 w-50">
          <input
            className={`form-control  ${
              nameError ? "border border-danger" : null
            }`}
            placeholder="Enter task details"
            type={"text"}
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
        </div>

        <div className="w-50 ">
          <select
            className="form-control "
            onChange={(e) => setDropDown(e.target.value)}
          >
            {rowTitles.map((r) => (
              <option key={r.key} value={r.key}>
                {r.title}
              </option>
            ))}
          </select>
        </div>
        <button type="submit" className="btn btn-primary">
          Add Task
        </button>
      </div>
    </form>
  );
};
export default AddForm;
