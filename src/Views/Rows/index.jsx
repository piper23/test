import React from "react";
import { Draggable } from "react-beautiful-dnd";
import CardInfo from "Views/CardInfo";
import Icon from "Components/Icon";

const getItemStyle = (isDragging, draggableStyle) => ({
  ...draggableStyle,
  userSelect: "none",
  zIndex: 2,
  background: isDragging ? "#f1f1f1" : "white",
});

function Rows({ data, stage, removeTask, updateTask }) {
  return (
    <div className="stage px-2 py-2 rounded-xl h-100">
      <div className={`list-group py-1 ${data.length ? "py-1" : "py-5"} `}>
        {data.map((task, index) => (
          <Draggable key={task.id} draggableId={task.id} index={index}>
            {(provided, snapshot) => (
              <div
                className={`list-group-item mb-2  ${stage}style onHovercard `}
                style={getItemStyle(
                  snapshot.isDragging,
                  provided.draggableProps.style
                )}
                ref={provided.innerRef}
                {...provided.draggableProps}
                {...provided.dragHandleProps}
              >
                <div className=" pointer d-flex align-items-center justify-content-between ">
                  <div className={task.text ? "col-6 " : "col-12"}>
                    <CardInfo
                      dragProp={provided.dragHandleProps}
                      updateTask={updateTask}
                      removeTask={removeTask}
                      task={task}
                      stage={stage}
                    />
                  </div>
                  {task.text && (
                    <React.Fragment>
                      <div className=" d-flex align-items-center justify-content-end">
                        <div
                          className="remove-btn btn"
                          onClick={() => removeTask({ taskID: task.id, stage })}
                        >
                          <Icon type="remove" height="20" width="20" />
                        </div>
                        <div>
                          <Icon type="drag" />
                        </div>
                      </div>
                    </React.Fragment>
                  )}
                </div>
              </div>
            )}
          </Draggable>
        ))}
      </div>
    </div>
  );
}

export default Rows;
