import React from "react";

function SearchForm({ updateSearchTerm, searchTerm }) {
  function handleTextChange(e) {
    updateSearchTerm(e.target.value);
  }

  return (
    <input
      className="form-control"
      type="text"
      value={searchTerm}
      placeholder="Search tasks..."
      onChange={handleTextChange}
    />
  );
}

export default SearchForm;
